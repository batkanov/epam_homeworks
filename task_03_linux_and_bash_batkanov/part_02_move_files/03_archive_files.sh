#!/bin/bash
show_help () {
   echo
   echo " Script usage:"
   echo " ./<script> <Result_Directory> <Directory_For_Archives>"
   echo
   echo "   <Result_Directory>         - directory with processed files"
   echo "   <Directory_For_Archives>   - archives will be stored there"
   echo
exit 0
}

check_slash () {
   dirToCheck=$1
   length=${#dirToCheck}
   last_char=${dirToCheck:length-1:1}
   if [ $last_char != "/" ]; then
     dirWithSlash=$(echo "$dirToCheck/")
   echo $dirWithSlash
   else
     echo $1
   fi
}

check_if_directory_exist () {
   echo
   echo "Checking provided paths:"
   if [ -d $1 ]; then
     echo
     echo "OK - Provided directory $1 exist!"
   else
     echo
     echo "NOT OK - Provided directory $1 doesn't exist! Please crate it!"
   fi
   if [ -d $2 ]; then
     echo
     echo "OK - Provided directory $2 exist!"
   else
     echo
     echo "NOT OK - Provided directory $2 doesn't exist! Please crate it!"
     exit 0
   fi
}

check_passed_arguments () {
   if [ -z $1 ] || [ -z $2 ]; then
     show_help
     else
     if [ -z $1 ]; then
        echo
        echo "No result dir supplied"
        show_help
        exit 0
     fi
     if [ -z $2 ]; then
        echo
        echo "No archive dir supplied"
        show_help
        exit 0
     fi
   fi
}

create_archive () {
   objectsArray=($(ls $1 | awk -F '-' '{print $1}' | sort -u))
   if [ -z "$(echo "${objectsArray[@]}")" ]; then
     echo "No files found in $1 dir, nothing to do"
     exit 0
   else
   echo
   re='^[0-9]+$'
   while true; do
     read -p "Enter count of days to archive: " daysCount

     if [ -z $daysCount ]; then
       echo "Please provide count of days!"
     elif ! [[ $daysCount =~ $re ]]; then
       echo "Provided values is not a number"
     elif [ $daysCount -ge 1000 ]; then
       echo "Too high value! Be reasonable! Enter value less than 1000!"
     else
       echo
       echo "Files older than $daysCount day(s) will be archived"
       break
     fi
   done

   archiveFromDate=$(date --date "$daysCount days ago" +%Y%m%d)
   archiveFromDateEpoch=$(date -d $archiveFromDate  +%s)

   filesToArchive=()
   datesToArchive=()

   echo
   echo "Creating archives..."

   for i in "${objectsArray[@]}"; do
     echo
     echo "Creating archive for $i object..."
     objectDates=($(ls $1$i* |  awk -F '-' '{print $4}' | sort -u ))
     for y in "${objectDates[@]}"; do
       fileDateEpoch=$(date -d $y +%s)
       if [[ $fileDateEpoch -le $archiveFromDateEpoch ]]; then
         fileNameToArchive=$(ls $1$i*$y*)
         datesToArchive=("${datesToArchive[@]}" "$y")
         filesToArchive=("${filesToArchive[@]}" "$fileNameToArchive")
       fi
     done
     if [ -z "$(echo "${filesToArchive[@]}")" ]; then
     echo "No files older than $daysCount day(s) from object $i"
     echo "Nothing to archive"
     else
     archiveStartDate=$(echo "${datesToArchive[@]}" | sort -u | sed 's/ /\n/g' | head -n 1)
     archiveEndDate=$(echo "${datesToArchive[@]}" | sort -u | sed 's/ /\n/g' | tail -n 1)
     archiveName=$(echo "$i-$archiveStartDate-$archiveEndDate.tar.gz")
     tar -zcf $2$archiveName $(echo "${filesToArchive[@]}")
     echo "$2$archiveName was created"
     echo "Files from object $i for $archiveStartDate-$archiveEndDate removed from result dir"
     rm $(echo "${filesToArchive[@]}")
     datesToArchive=()
     filesToArchive=()
     fi
   done
 fi
}

resultDir=$1
archiveDir=$2

check_passed_arguments $resultDir $archiveDir
resultDir=$(check_slash $resultDir)
archiveDir=$(check_slash $archiveDir)
check_if_directory_exist $resultDir $archiveDir
create_archive $resultDir $archiveDir
