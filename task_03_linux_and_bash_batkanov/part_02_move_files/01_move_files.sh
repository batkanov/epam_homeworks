#!/bin/bash
show_help () {
   echo
   echo " Script usage:"
   echo " ./<script> <Initial_Directory> <Result_Directory>"
   echo
   echo "   <Initial_Directory>   - directory with incoming files"
   echo "   <Result_Directory>    - files will be moved to this directory"
   echo
exit 0
}

check_slash () {
   dirToCheck=$1
   length=${#dirToCheck}
   last_char=${dirToCheck:length-1:1}
   if [ $last_char != "/" ]; then
     dirWithSlash=$(echo "$dirToCheck/")
   echo $dirWithSlash
   else
     echo $1
   fi
}

check_if_directory_exist () {
   echo
   echo "Checking provided paths:"
   if [ -d $1 ]; then
     echo
     echo "OK - Provided directory $1 exist!"
   else
     echo
     echo "NOT OK - Provided directory $1 doesn't exist! Please crate it!"
   fi
   if [ -d $2 ]; then
     echo
     echo "OK - Provided directory $2 exist!"
   else
     echo
     echo "NOT OK - Provided directory $2 doesn't exist! Please crate it!"
     exit 0
   fi
}

check_passed_arguments () {
   if [ -z $initialDir ] || [ -z $resultDir ]; then
     show_help
     else
     if [ -z $initialDir ]; then
        echo
        echo "No initial dir supplied"
        show_help
        exit 0
     fi
     if [ -z $resultDir ]; then
        echo
        echo "No result dir supplied"
        show_help
        exit 0
     fi
   fi
}

check_if_directories_are_the_same () {
 if [ $initialDir -ef $resultDir ]; then
   echo
   echo "Initial directory and result directory are equal! Please provide another path for one of them!"
   exit 0
 fi
}

move_files () {
   echo
   echo "Moving files..."
   if [ $(ls $initialDir | wc -l) -ne 0 ]; then
     readlink -f $1* | while read file; do
       moveDate=$(date '+%Y-%m-%d %H:%M:%S')
       checkLsof=$(lsof | grep $file | wc -l)
       if [ $checkLsof -eq 0 ]; then
         firstEntry=$(head -n 1 $file)
         objectName=$(echo $firstEntry | awk '{print $1}')
         objectDate=$(echo $firstEntry | awk '{print $2" "$3}' | sed 's/://g' | sed 's/-//g' | sed 's/ /-/g')
         fileReceivingDate=$(ls -lt --time-style=full-iso $file | awk '{print $6" "$7}' | sed 's/\.[0-9]*//' | sed 's/://g' |  sed 's/-//g' |  sed 's/ /-/g')
         fileName=$(echo "$objectName-$objectDate-$fileReceivingDate")
         checkFirstEntry=$(cat $file | wc -l)
           if [ $checkFirstEntry == 0 ]; then
             echo "File $file is empty, will not be moved"
           else
             mv $file $resultDir$fileName
             echo "$moveDate: File $file moved to $resultDir$fileName" >> $logFile
             echo "$objectName,$fileReceivingDate,$file" >> $statFile
           fi
       else
         echo "File $file in use, can't be moved right now."
         echo "$moveDate: ! ! ! File $file in use, can't be moved right now" >> $logFile
       fi
     done
     echo
     echo "Done."
     echo "Log file: $logFile"
     echo
   else
     echo "No files to move in $initialDir"
   fi
}

report () {
   readarray movedArray < $statFile
   rm $statFile
   filesMoved=${#movedArray[*]}
   echo "Files moved: $filesMoved"

   IFS=$'\n'
   objectsArray=($(echo "${movedArray[@]}" | awk -F ',' '{print $1}' | sed 's/ //g' | sort -u))
   unset IFS
   count=0
   echo
   echo "Count of fully received and moved files per object:"
   for i in "${objectsArray[@]}"; do
      for y in "${movedArray[@]}"; do
         checkObject=$(echo $y | awk -F ',' '{print $1}')
         if [ "$i" == "$checkObject" ]; then
         let count=$((count + 1))
         fi
      done
      echo "$i = $count"
      count=0
   done
}


initialDir=$1
resultDir=$2

Date=$(date '+%Y-%m-%d_%H-%M-%S')
logFile=$(echo "moved_files.$Date.log")
statFile=$(echo "stat.$Date.csv")

check_passed_arguments $initialDir $resultDir
initialDir=$(check_slash $initialDir)
resultDir=$(check_slash $resultDir)
check_if_directory_exist $initialDir $resultDir
check_if_directories_are_the_same $initialDir $resultDir

move_files $initialDir $resultDir
report
