#!/bin/bash
newMysqlRootPass=$(echo "MyPassword1+")

echo "1. Installing Percona Server..."
echo "------------------------------------------------------"
echo
sudo yum install -y -q -e 0 https://www.percona.com/redir/downloads/percona-release/redhat/latest/percona-release-0.1-6.noarch.rpm
sudo yum install -y -q -e 0 Percona-Server-server-57

echo
echo "2. Starting Percona Server..."
echo "------------------------------------------------------"
echo
sudo systemctl start mysqld
sudo systemctl enable mysqld

echo
echo "3. Changing root password (New password is $newMysqlRootPass)"
echo "------------------------------------------------------"
echo
mysqlTempPassword=$(sudo grep 'temporary password' /var/log/mysqld.log | awk '{print $11}')
mysqladmin -u root -p$mysqlTempPassword password $newMysqlRootPass
##sudo mysql --user=root --password="$mysqlTempPassword" -Bse "ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyPassword1+', 'root'@'localhost' PASSWORD EXPIRE NEVER;"

echo
echo "4. Installing PHP..."
echo "------------------------------------------------------"
echo
sudo yum install -y -q -e 0 php php-mysql

echo
echo "5. Installing Apache Web Server..."
echo "------------------------------------------------------"
echo
sudo yum install -y -q  -e 0 httpd

echo
echo "6. Installing PHP My Admin..."
echo "------------------------------------------------------"
echo
sudo yum install -y -q -e 0 phpmyadmin
sudo sed -i '17i Require all granted' /etc/httpd/conf.d/phpMyAdmin.conf
sudo sed -i '35i Require all granted' /etc/httpd/conf.d/phpMyAdmin.conf
sudo systemctl start -q httpd

echo
echo "Done!"
echo "check http://<Server_IP>/phpmyadmin/"
echo "Login: root"
echo "Password: $newMysqlRootPass"
