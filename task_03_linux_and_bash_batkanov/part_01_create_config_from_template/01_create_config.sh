#!/bin/bash
show_help () {
   echo ""
   echo " Script usage:"
   echo " ./<script> <Template_File> <Result_File>"
   echo ""
   echo "   <Template_File>   - template file"
   echo "   <Result_File>     - configuration file which will be created"
   echo ""
exit 0
}

check_if_template_exist () {
 if [ ! -f $templateFile ]; then
    echo ""
    echo "Template file \"$templateFile\"  not found!"
    echo "Please provide a valid template file!"
    echo ""
    exit 0
 else
    echo ""
    echo "Template file is: $templateFile"
 fi
}

create_result_file () {
 if [ ! -f $resultFile ]; then
    touch "$resultFile"
    echo ""
    echo "Result file is: $resultFile"
 else
    echo ""
    echo "Result file $resultFile is already exist!"
    echo ""
    echo "Please delete it or provide another filename:" 
    echo " 1. Delete existing $resultFile and create a new one using provided name"
    echo " 2. I want to provide another filename for result file"
    echo " 0. Exit from script"
    
    while true; do
        read -p "Press 1, 2 or 0: " -n 1 -r
        echo
        
        if [[ $REPLY =~ ^[1]$ ]]; then
	    echo ""
	    echo "Deleting existing file \"$resultFile\"..."
            rm -f $resultFile
            echo ""
	    echo "Empty file \"$resultFile\" created"
            touch $resultFile
            break
        fi

        if [[ $REPLY =~ ^[2]$ ]]; then
            while true; do
               read -p "New filename is: " resultFile
               if [ ! -f $resultFile ]; then
                  echo "Creating a new result file with name $resultFile..."
                  touch $resultFile
                  echo "New result file is: $resultFile"
                  break
               else
		  echo "File $resultFile already exists too. Enter another name"
            fi 
            done
         fi

         if [[ $REPLY =~ ^[0]$ ]]; then
         exit 0
         fi
    done
 fi
}

check_passed_arguments () {
 if [ -z $templateFile ] || [ -z $resultFile ]; then
     show_help
     else
     if [ -z $templateFile ]; then
        echo "No template file supplied"
	show_help
     else
        check_if_template_exist $templateFile
     fi

     if [ -z $resultFile ]; then
        echo "No result file supplied"
	show_help
     else
	create_result_file $resultFile
     fi
fi
}

create_config () {
 cat $templateFile | while read -r line; do
 while [[ "$line" =~ (\$\{[a-zA-Z_][a-zA-Z_0-9]*\}) ]] ; do
        LHS=${BASH_REMATCH[1]}
        RHS="$(eval echo "\"$LHS\"")"
        line=${line//$LHS/$RHS}
    done
    echo "$line" >> $resultFile
 done
 echo ""
 echo "Done!" 
 echo "Please check \"$resultFile\" file"
}

templateFile=$1
resultFile=$2
check_passed_arguments $templateFile $resultFile
create_config $templateFile $resultFile
