#!/bin/bash
x=0
y=9
search=""

 while [[ $x -le 100 ]]; do
     if (( x < 100 )); then
       count=$(awk 'BEGIN { for (i=1;i<201;i++) print int (101*rand())}' | sed -e "/^$search[0-9]\{1\}$/!d" | sed -n '$=')
       printf "$x-$y: "$count" "; printf "*%0.s" $(seq $count); printf "\n"
     else
       count=$(awk 'BEGIN { for (i=1;i<201;i++) print int (101*rand())}' | sed -e "/^1[0-9]\{2\}$/!d" | sed -n '$=')
       printf "$x: "$count" "; printf "*%0.s" $(seq $count); printf "\n"
     fi

   let search=$(($search + 1))
   let x=$(($x + 10))
   let y=$(($y + 10))
 done
exit 0
