#!/bin/bash
x=0
y=9
search=""

 while [[ $x -le 100 ]]; do
     if (( x < 100 )); then
       count=$(awk 'BEGIN { for (i=1;i<201;i++) print int (101*rand())}' | sed -e "/^$search[0-9]\{1\}$/!d" | sed -n '$=')
       percent=$(echo $count | awk '{percent=100*$0/200; print percent}')
       printf "$x-$y: "$percent" "; printf "*%0.s" $(seq $percent); printf "\n"
     else
       count=$(awk 'BEGIN { for (i=1;i<201;i++) print int (101*rand())}' | sed -e "/^1[0-9]\{2\}$/!d" | sed -n '$=')
       percent=$(echo $count | awk '{percent=100*$0/200; print percent}')
       printf "$x: "$percent" "; printf "*%0.s" $(seq $percent); printf "\n"
     fi

   let search=$(($search + 1))
   let x=$(($x + 10))
   let y=$(($y + 10))
 done
exit 0
