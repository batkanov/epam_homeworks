#!/bin/bash

## ACL Task, student Igor Batkanov

# Create Proj[1-3] directories with sticky bit
for i in {1..3}; do
  dir=$(echo "/tmp/Proj$i") 
  sudo mkdir $dir
  sudo chmod 1750 $dir
done

# Create R[1-5], I[1-3], A[1-4] users
for i in {1..5}; do
  user=$(echo "R$i")
  sudo useradd $user 
done

for i in {1..3}; do
  user=$(echo "I$i")
  sudo useradd $user
done

for i in {1..4}; do
  user=$(echo "A$i")
  sudo useradd $user
done

# Create ACL
sudo setfacl -d -m u:R2:rwx,u:R3:rwx,u:R5:rwx,u:A1:rwx,u:A4:rx,u:I1:rwx,u:I2:rwx,u:I3:rwx /tmp/Proj1
sudo setfacl -m u:R2:rwx,u:R3:rwx,u:R5:rwx,u:A1:rwx,u:A4:rx,u:I1:rwx,u:I2:rwx,u:I3:rwx /tmp/Proj1
sudo setfacl -d -m u:R1:rwx,u:R5:rwx,u:A1:rwx,u:A2:rx,u:A3:rx,u:I1:rwx,u:I2:rwx,u:I3:rwx /tmp/Proj2
sudo setfacl -m u:R1:rwx,u:R5:rwx,u:A1:rwx,u:A2:rx,u:A3:rx,u:I1:rwx,u:I2:rwx,u:I3:rwx /tmp/Proj2
sudo setfacl -d -m u:R1:rwx,u:R2:rwx,u:R4:rwx,u:A2:rwx,u:A1:rx,u:A4:rx,u:I1:rwx,u:I2:rwx,u:I3:rwx /tmp/Proj3
sudo setfacl -m u:R1:rwx,u:R2:rwx,u:R4:rwx,u:A2:rwx,u:A1:rx,u:A4:rx,u:I1:rwx,u:I2:rwx,u:I3:rwx /tmp/Proj3
