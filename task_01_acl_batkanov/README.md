Практика раздачи прав в Linux
Студент: Игорь Батьканов

Задание реализовал через acl + sticky bit
acl_task.sh - создаёт директории, пользователей, вешает acl на директории
acl_clean.sh - удаляет созданные директории и пользователей

Что касается реализации самого задания

1. Штатными средствами Linux не организовать наследование sticky bit на 
вложенные директории. Можно сделать скрипт, который с заданным интервалом в
cron будет проверять наличие вложенных директорий без sticky bit проставлять его.
Нужно ли это в рамках задания?
(Как вариант можно еще использовать inotifywait вместо скрипта в cron)

2. Sticky bit не совсем удовлетворяет заданию, т.к. информационным менеджерам 
надо запретить что-либо удалять. В моей реализации информационный менеджер
может удалять файл, владельцем которого он является.

3. С помощью "chattr +a <dir name>" можно реализовать создание файлов в директории 
без возможности их удаления, но это будет распространятся на всех пользователей
(вопрос с наследованием надо будет решать как и в пункте 1).

4. Пытался реализовать задание с помощью bindfs, но чуть не сошел с ума :(
