#!/bin/bash

sudo rm -rf /tmp/Proj1
sudo rm -rf /tmp/Proj2
sudo rm -rf /tmp/Proj3

for i in {1..5}; do
 user=$(echo "R$i")
 sudo userdel -r $user 
done

for i in {1..3}; do
 user=$(echo "I$i")
 sudo userdel -r $user
done

for i in {1..4}; do
 user=$(echo "A$i")
 sudo userdel -r $user
done
